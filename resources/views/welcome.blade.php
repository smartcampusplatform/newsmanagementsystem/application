@extends('layouts.app', ['class' => 'off-canvas-sidebar index-page sidebar-collapse', 'activePage' => 'home', 'title' => __('News Management System')])

@section('content')
    {{--<div class="container" style="height: auto;">--}}
    <div class="row justify-content-center">
        <div class="section" id="carousel">
            {{--<h1 class="text-white text-center">{{ __('News Management System') }}</h1>--}}
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img class="d-block" src="{{ asset('material') }}/img/itb/1.jpg" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block" src="{{ asset('material') }}/img/itb/2.jpg" alt="Second slide">
                        <div class="carousel-caption d-none d-md-block">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block" src="{{ asset('material') }}/img/itb/3.jpg" alt="Third slide">
                        <div class="carousel-caption d-none d-md-block">
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <i class="now-ui-icons arrows-1_minimal-left"></i>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <i class="now-ui-icons arrows-1_minimal-right"></i>
                </a>
            </div>
        </div>
        <div class="container-fluid">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header card-header-info">
                        <h4 class="card-title">{{ __('News Management System - Daftar Berita') }}</h4>

                        {{--<button onclick="getLocation()">Try It</button>--}}
                        {{--<p id="demo"></p>--}}
                    </div>

                    <div class="row card-body">
                        <?php
                        use GuzzleHttp\Client;
                        $client = new Client();
                        $res = $client->request('GET', 'http://localhost:8000/api/news');
                        $model=json_decode($res->getBody());
                        $model=array_reverse($model);
                        $i = 0;
                        ?>

                        <script type="application/javascript">
                            window.onload = function init() {
                                fetch('http://localhost:8000/api/news')
                                    .then(response => response.json())
                                    .then(data => console.log('data is', data))
                                    .catch(error => console.log('error is', error));}
                        </script>

                        @foreach($model as $berita)
                            <?php
                            if(++$i > 6) break;
                            $string = strip_tags($berita->content);
                            if (strlen($string) > 500) {

                                // truncate string
                                $stringCut = substr($string, 0, 500);
                                $endPoint = strrpos($stringCut, ' ');

                                //if the string doesn't contain any space then it will cut without word basis.
                                $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                $string .= "...";
                            }
                            ?>
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-header card-header-primary">
                                        <a href="#" ><h4 class="card-title">{{ __($berita->title)." - Baca Selengkapnya" }}</h4></a>
                                    </div>
                                    <div class="card-body">
                                        <div class="col-12 text-justify">
                                            <p>{{ __($string) }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--</div>--}}
@endsection
