<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 22/04/2019
 * Time: 11:55
 */

?>
@extends('layouts.app', ['activePage' => 'fakultas', 'titlePage' => __('Fakultas')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-success">
                            <h4 class="card-title ">Daftar Web Fakultas</h4>
                        </div>
                        <div class="card-body">
                            @if (session('status'))
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="alert alert-success">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <i class="material-icons">close</i>
                                            </button>
                                            <span>{{ session('status') }}</span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-12 text-right">
                                    <a href="{{ route('user.create') }}" class="btn btn-sm btn-primary">{{ __('Add user') }}</a>
                                </div>
                            </div>
                            <?php
                            //                  dd($classroom);

                            use GuzzleHttp\Client;
                            $client = new Client();
                            $res = $client->request('GET', 'http://localhost:8000/api/faculties');
                            $model=json_decode($res->getBody());

                            ?>
                            <script type="application/javascript">
                                window.onload = function init() {
                                    fetch('http://localhost:8000/api/faculties',{'mode': 'no-cors'})
                                        .then(response => response.json())
                                        .then(data => console.log('data is', data))
                                        .catch(error => console.log('error is', error));}
                            </script>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                    <th>
                                        {{ __('ID') }}
                                    </th>
                                    <th>
                                        {{ __('Nama Fakultas') }}
                                    </th>
                                    <th>
                                        {{ __('Url Web') }}
                                    </th>
                                    <th>
                                        {{ __('Nama Tag') }}
                                    </th>
                                    </thead>
                                    <tbody>
                                    @foreach($model as $m)
                                        <tr>
                                            <td>
                                                {{ $m->id}}
                                            </td>
                                            <td>
                                                {{ $m->nama_fakultas}}
                                            </td>
                                            <td>
                                                {{ $m->url_web}}
                                            </td>
                                            <td>
                                                {{ $m->nama_tag}}
                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




