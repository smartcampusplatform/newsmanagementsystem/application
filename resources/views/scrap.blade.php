<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 22/04/2019
 * Time: 11:55
 */

?>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4-4.1.1/jq-3.3.1/dt-1.10.18/datatables.min.css"/>
</head>
<body>
@extends('layouts.app', ['activePage' => 'scrap', 'titlePage' => __('Daftar Berita Scrap')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Daftar Berita Web Fakultas</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>id</th>
                    <th>date</th>
                    <th>sumber</th>
                    <th>status</th>
                    <th>title</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>id</th>
                    <th>date</th>
                    <th>sumber</th>
                    <th>status</th>
                    <th>title</th>
                </tr>
                </tfoot>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{--<div class="modal" id="modal">--}}
{{--<div class="modal-dialog">--}}
{{--<div class="modal-content">--}}
{{--<div class="modal-header">--}}
{{--<h4 class="modal-title">QRCode</h4>--}}
{{--<input id="serialNumber" type="hidden">--}}
{{--<input id="barang" type="hidden">--}}
{{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--</div>--}}
{{--<div class="modal-body">--}}
{{--</div>--}}
{{--<div class="modal-footer">--}}
{{--<button type="button" class="btn btn-success" data-dismiss="modal">Print</button>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
</body>
</html>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.0.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4-4.1.1/jq-3.3.1/dt-1.10.18/datatables.min.js"></script>
<script>

    $.ajax({dataType: "json", url: "http://localhost/newsMS/services/public/api/scrap"}).done(function (data) {

        var scrap_data = '';
        $.each(data, function (key, value) {
            scrap_data += '<tr>';
            scrap_data += '<td>' + value.id + '</td>';
            scrap_data += '<td>' + value.date + '</td>';
            scrap_data += '<td>' + value.guid.rendered + '</td>';
            scrap_data += '<td>' + value.status + '</td>';
            scrap_data += '<td>' + value.title.rendered + '</td>';
            scrap_data += '</tr>';
        });
        $('#dataTable').append(scrap_data);

    });


</script>




