<div class="sidebar" data-color="green" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="{{route('home')}}" class="simple-text logo-normal">
      {{ __('News Management') }}
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">

      <li class="nav-item{{ $activePage == 'table' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('table') }}">
          <i class="material-icons">content_paste</i>
            <p>{{ __('Daftar Agenda') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'typography' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('typography') }}">
          <i class="material-icons">library_books</i>
            <p>{{ __('Jadwal Kegiatan') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'icons' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('icons') }}">
          <i class="material-icons">bubble_chart</i>
          <p>{{ __('Komentar Berita') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'fakultas' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('fakultas') }}">
          <i class="material-icons">bubble_chart</i>
          <p>{{ __('Daftar Fakultas Web') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'scrap' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('scrap') }}">
          <i class="material-icons">bubble_chart</i>
          <p>{{ __('Daftar Scrap Berita') }}</p>
        </a>
      </li>
    </ul>
  </div>
</div>