<div id="navbar" class="navbar-expand-lg fixed-top " color-on-scroll="400">
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg bg-info">
    <div class="container">
      <div class="navbar-wrapper">
        <a class="navbar-brand" href="{{ route('home') }}">{{ $title }}</a>
      </div>
      <div class="collapse navbar-collapse justify-content-end">
        <ul class="navbar-nav">
          <li class="nav-item{{ $activePage == 'login' ? ' active' : '' }}">
            <a href="{{ route('login') }}" class="nav-link">
              <i class="material-icons">fingerprint</i> {{ __('Login') }}
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</div>
<!-- End Navbar -->